<?php $this->load->view('includes/file_header'); ?>
<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div style="padding-top: 15rem;">
<h2><?php echo lang('login_heading');?></h2>
<p><?php echo lang('login_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/login", 'class="form-horizontal"');?>

  <div class="form-group">
    <?php echo lang('login_identity_label', 'identity', 'class="col-sm-3 control-label"');?>
    <div class="col-sm-9">
      <?php echo form_input($identity, '', 'class="form-control"');?>
    </div>
  </div>

  <div class="form-group">
    <?php echo lang('login_password_label', 'password', 'class="col-sm-3 control-label"');?>
    <div class="col-sm-9">
      <?php echo form_input($password, '', 'class="form-control"');?>
    </div>
  </div>

  <p><?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-success"');?></p>

<?php echo form_close();?>

<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>

</div>
</div>

<?php $this->load->view('includes/file_footer'); ?>