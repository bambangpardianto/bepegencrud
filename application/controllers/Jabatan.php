<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends BP_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->tbl = 'data_jabatan';
		$this->url_ = 'jabatan';
		$this->pk = 'id_jabatan';
		$this->dis_type = ['id_jabatan'];
		
	}

}

/* End of file Jabatan.php */
/* Location: .//E/xampp/htdocs/bepegencrud/penggajian/controllers/Jabatan.php */ ?>