<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends BP_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->tbl = 'master_menu';
		$this->url_ = 'menu';
		$this->pk = 'id';
		$this->dis_type = ['id'];
	}

}

/* End of file Menu.php */
/* Location: .//E/xampp/htdocs/bepegencrud/penggajian/controllers/Menu.php */ ?>