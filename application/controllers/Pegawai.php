<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends BP_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->tbl = 'data_pegawai';
		$this->url_ = 'pegawai';
		$this->pk = 'id_pegawai';
		$this->dis_type = ['id_pegawai'];
		$this->type = [
			'nik'=>'number',
			'nama_pegawai'=>'text',
			'jenis_kelamin'=>'select',
			'jabatan'=>'select',
			'tanggal_masuk'=>'date',
			'status'=>'select',
			'photo'=>'file'
		];
	}

}

/* End of file Pegawai.php */
/* Location: .//E/xampp/htdocs/bepegencrud/penggajian/controllers/Pegawai.php */ ?>