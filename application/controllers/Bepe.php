<?php 
	
	defined('BASEPATH') OR exit('No direct script access allowed');

	/*
	| Copy this Controller for reference.
	*/

	class Bepe extends BP_Controller {
		
		/*
		| -------------------------------------------------------------------
		| Global Conf
		| -------------------------------------------------------------------
		|
		| Table name, default URI, primary key.
		|
		| Disable Column on table when you want.
		|
		| Use Datatable or Pagination. Default true, change to false for use pagination
		| 
		*/

		protected $tbl 				= 'users';			// Rename with table name.
		protected $url_ 			= 'anggota'; 			// Rename must same with controller name.
		protected $pk 				= 'id';				// Rename with PK (Primary Key table).

		protected $dis_type 		= ['id'];			// Add/Remove with Field Name table / column name.

		protected $data_table 		= true;				// Activate datatable. (Konvensional as soon as).

		/*
		| -------------------------------------------------------------------
		| Configuration Column/Field Table
		| -------------------------------------------------------------------
		|
		| Data By id on joined table Column and Rename Text Column
		| Show Data on table when value is using by join to another table. Default is empty or array();
		| Rename header text table, just setting by field name then change to u want.
		|
		| protected $read_joined  	= [	
		|								['users','username',['user_id']],
		|								['groups','name',['group_id']],
		|						  	  ];
		|
		| protected $ren_col   	  	= [ 'field1' => 'new name field 1',
		|							  	'field2' => 'new name field 2',
		|							  	'Dan Seterusnya...'
		| 							  ];
		*/

		protected $ren_col 			= [];				// Change view thead table

		protected $read_joined 		= [];				// Configuration for id/joined value to another table.

		/*
		| -------------------------------------------------------------------
		| Configuration Create, Read, Edit
		| -------------------------------------------------------------------
		|
		| Disable Input Form By Field name. (on Table show => $dis_type)
		|
		| Form (using select).
		| If using input type select must configure $cmb_d with [field_name=>[table joined,name,pk], ...]
		| Example :
		|
		| protected $cmb_d 			= [
		|								'user_id'=>['users','username','id'],
		|								'group_id'=>['groups','name','id']
		|						  	  ];
		|
		| Change Type Input form.
		| type input like : text, hidden, date, number, password, checkbox, radio, other...
		| type input default is text.
		| Example :
		|
		| protected $type 			= ['field_nm'=>'text','field_nm'=>'select','field_nm'=>'date',{or other}]
		|
		| Rename Text Label Form
		| Example :
		|
		| protected $ren_label   	= [ 'field1' => 'new name field 1',
		|							  	'field2' => 'new name field 2',
		|							  	'Dan Seterusnya...'
		| 							  ];
		| Deprecated.
		| $tbl_joined = ''
		| $field_joined = ''
		*/

		protected $cmb_d 			= []; 				// Configuration Form with select dinamic.

		protected $type 			= [];				// Configuration type form input.

		protected $ren_label 		= [];				// Configuration Label form input.

		public function __construct()
		{
			parent::__construct();
		}
	
	}
	
	/* End of file Bepe.php */
	/* Location: ./application/controllers/Bepe.php */
 ?>